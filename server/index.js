require('dotenv').config();
const cors = require('cors');
const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');

const sequelize = require('./db');
const router = require('./routes/index');
const models = require('./models/models');
const errorHandler = require('./middleware/ErrorHandlingMiddleware');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static(path.resolve(__dirname, 'static')));
app.use(fileUpload({}))
app.use('/api', router);

// Обработка ошибок, последний MiddleWare
app.use(errorHandler);

app.get('/', (req, res)=> {
    res.status(200).json({ message: 'Work'} )
})

const start = async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync();
        app.listen(process.env.PORT, () => console.log(`Server started on port ${process.env.PORT}`))
    } catch (e) {
        console.log(e);
    }
}
start();
