import React, { useContext } from 'react';
import { Context } from "../index";
import { Navbar, Container, Nav, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { ADMIN_ROUTE, LOGIN_ROUTE, SHOP_ROUTE } from "../constRoutes";
import { observer } from "mobx-react-lite";
import { useHistory } from "react-router-dom";

const NavBar = observer(() => {
    const { user } = useContext(Context);
    const history = useHistory();

    const logOut = () => {
        user.setUser({});
        user.setIsAuth(false);
    }

    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <NavLink style={{color: 'white', textDecoration: 'none', }} to={SHOP_ROUTE}>Онлайн-Маркет</NavLink>
                {user.isAuth
                    ?   <Nav className="ml-auto" style={{color: 'white'}}>
                            <Button variant={'outline-light'}
                                    onClick={() => history.push(ADMIN_ROUTE)}>Панель администратора</Button>
                            <Button variant={'outline-light'}
                                    onClick={() => logOut()}
                                    style={{marginLeft: '10px'}}>Выйти</Button>
                        </Nav>
                    : <Button variant={'outline-light'} onClick={() => history.push(LOGIN_ROUTE)}>Авторизация</Button> }
            </Container>
        </Navbar>
    );
});

export default NavBar;