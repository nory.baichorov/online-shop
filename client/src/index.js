import React, { createContext } from 'react';
import App from './App';
import ReactDOM from 'react-dom';
import UserStore from "./store/UserStore";
import DeviceStore from "./store/DeviceStore";

export const Context = createContext(null);

ReactDOM.render(
  <React.StrictMode>
      <Context.Provider value={{
          user: new UserStore(),
          device: new DeviceStore()
      }}>
          <App />
      </Context.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
